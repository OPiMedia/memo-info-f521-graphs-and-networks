DOCUMENTS = INFO_F521_Graphs_and_networks_2016__memo.pdf

.SUFFIXES:
.SUFFIXES:	.dvi .idx .ind .pdf .ps

DVIPS      = dvips
DVIPSFLAGS =

MAKEINDEX      = makeindex
MAKEINDEXFLAGS =

PSPDF      = ps2pdf
PSPDFFLAGS = -sPAPERSIZE=a4

TEX      = latex
TEXFLAGS =


PSBOOK      = psbook
PSBOOKFLAGS =

PSNUP      = psnup
PSNUPFLAGS =

PSRESIZE      = psresize
PSRESIZEFLAGS =


RM    = rm -f
SHELL = sh



###
# #
###
.PHONY:	all booklet

all:	$(DOCUMENTS)

booklet:	INFO_F521_Graphs_and_networks_2016__memo__booklet.pdf

INFO_F521_Graphs_and_networks_2016__memo__booklet.ps:	INFO_F521_Graphs_and_networks_2016__memo.ps
	$(PSBOOK) $(PSBOOKFLAGS) $< \
	| $(PSNUP) $(PSNUPFLAGS) -2 -W421 -H595 -w595 -h842 \
	| $(PSRESIZE) $(PSRESIZEFLAGS) -W595 -H842 -w595 -h842 > $@



################
# Dependencies #
################
INFO_F521_Graphs_and_networks_2016__memo.dvi:	opimemo.sty chapter_1.tex chapter_2.tex chapter_3.tex chapter_4.tex chapter_5.tex chapter_6.tex references.tex



#########
# Rules #
#########
.PRECIOUS: %.dvi %.idx %.ind %.ps

%.dvi:	%.tex %.ind
	$(TEX) $(TEXFLAGS) $<

%.idx:	%.tex
	$(TEX) $(TEXFLAGS) $<

%.ind:	%.idx
	$(MAKEINDEX) $(MAKEINDEXFLAGS) $<

%.ps:	%.dvi
	$(DVIPS) $(DVIPSFLAGS) -o $@ $<

%.pdf:	%.ps
	$(PSPDF) $(PSPDFFLAGS) $< $@



#########
# Clean #
#########
.PHONY:	clean cleanDvi cleanPdf cleanPs distclean overclean

clean:	cleanDvi
	$(RM) *.aux *.bbl *.blg *.brf *.idx *.ilg *.ind *.loe *.log .log *.out *.toc *.tdo

cleanDvi:
	$(RM) *.dvi

cleanPdf:
	$(RM) *.pdf

cleanPs:
	$(RM) *.ps

distclean:	clean

overclean:	distclean  cleanPdf cleanPs
