.. -*- restructuredtext -*-

====================================
Memo INFO-F521 *Graphs and networks*
====================================

**Memo**
about INFO-F521
*Graphs and networks*
(course by Gwenaël Joret, ULB, 2016)


* `PDF document`_
* `Booklet format in PDF`_
* `Booklet format in PostScript`_
* `LaTeX sources`_

.. _`Booklet format in PDF`: https://bitbucket.org/OPiMedia/memo-info-f521-graphs-and-networks/raw/master/memo/INFO_F521_Graphs_and_networks_2016__memo__booklet.pdf
.. _`Booklet format in PostScript`: https://bitbucket.org/OPiMedia/memo-info-f521-graphs-and-networks/raw/master/memo/INFO_F521_Graphs_and_networks_2016__memo__booklet.ps
.. _`LaTeX sources`: https://bitbucket.org/OPiMedia/memo-info-f521-graphs-and-networks/src/master/memo/
.. _`PDF document`: https://bitbucket.org/OPiMedia/memo-info-f521-graphs-and-networks/raw/master/memo/INFO_F521_Graphs_and_networks_2016__memo.pdf

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png

|



|Memo|

(picture_ on Wikipedia, A 3-coloring of the Petersen graph's vertices.)

.. _picture: https://en.wikipedia.org/wiki/File:Petersen_graph_3-coloring.svg

.. |Memo| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/12/3635780461-3-memo-info-f521-graphs-and-networks-lo_avatar.png
